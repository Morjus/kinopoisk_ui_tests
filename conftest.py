import pytest
import allure
from selenium import webdriver
from selenium.webdriver.support.events import EventFiringWebDriver, AbstractEventListener
import logging
from dotenv import load_dotenv
from allure_commons.types import AttachmentType

load_dotenv()
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO, filename="test.log")


class SeleniumListener(AbstractEventListener):

    def before_click(self, element, driver):
        logging.info(f"I'm clicking {element}")

    def after_click(self, element, driver):
        logging.info(f"I've clicked {element}")

    def before_find(self, by, value, driver):
        logging.info(f"I'm looking for '{value}' with '{by}'")

    def after_find(self, by, value, driver):
        logging.info(f"I've found '{value}' with '{by}'")

    def before_quit(self, driver):
        logging.info(f"I'm getting ready to terminate {driver}")

    def after_quit(self, driver):
        logging.info(f"Driver closed.")

    def on_exception(self, exception, driver):
        logger.error(f'Oooops i got: {exception}')
        allure.attach(driver.get_screenshot_as_png(), name=f"{driver.session_id}.png",
                      attachment_type=AttachmentType.PNG)


def pytest_addoption(parser):
    parser.addoption("--browser", action="store", default="chrome")
    parser.addoption("--version_browser", action="store", default="84.0")
    parser.addoption("--selenoid", action="store", default="localhost")


@pytest.fixture
def browser(request):
    browser = request.config.getoption("--browser")
    selenoid = request.config.getoption("--selenoid")
    version_browser = request.config.getoption("--version_browser")

    executor_url = f"http://{selenoid}:4444/wd/hub"

    caps = {"browserName": browser,
            "version": version_browser,
            "enableVnc": True,
            "enableVideo": True,
            "enableLog": True,
            "screenResolution": "1920x1080x24",
            "name": request.node.name}

    driver = webdriver.Remote(command_executor=executor_url, desired_capabilities=caps)

    driver = EventFiringWebDriver(driver, SeleniumListener())
    with allure.step("Start chrome browser for test."):

        def fin():
            try:
                allure.attach(name=driver.session_id,
                              body=str(driver.desired_capabilities),
                              attachment_type=allure.attachment_type.JSON)
                allure.attach(name="chrome log",
                              body=driver.get_log('browser'),
                              attachment_type=allure.attachment_type.TEXT)
            except TypeError as e:
                logger.error(f'Oooops i got: {e}')
            finally:
                with allure.step("Closing browser."):
                    driver.quit()

        request.addfinalizer(fin)
    return driver
